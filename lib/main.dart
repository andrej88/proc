import "package:english_words/english_words.dart";
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Proc',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: Colors.indigo,
      ),
      home: new ProcWidget(),
    );
  }
}

class ProcWidget extends StatefulWidget {
  @override
  State createState() => new ProcWidgetState(Section.CHECKLISTS);
}

class ProcWidgetState extends State<ProcWidget> {
  final _templates = const <String>["Sample 1", "Sample 2", "Sample 3"];

  final _activeChecklists = const <String>["I am active", "Me too"];

  Section _section;

  Widget _inboxBacking;
  Widget get _inbox {
    if (_inboxBacking == null) {
      _inboxBacking = _buildInbox();
    }
    return _inboxBacking;
  }

  Widget _templateViewBacking;
  Widget get _templateView {
    if (_templateViewBacking == null) {
      _templateViewBacking = _buildTemplateList();
    }
    return _templateViewBacking;
  }


  ProcWidgetState(this._section);

  @override
  Widget build(BuildContext context) => new Scaffold(
        bottomNavigationBar: new BottomNavigationBar(
          items: [
            new BottomNavigationBarItem(icon: new Icon(Icons.inbox), title: new Text("Inbox")),
            new BottomNavigationBarItem(icon: new Icon(Icons.list), title: new Text("Templates")),
          ],
          onTap: (i) {
            setState(() {
              _section = Section.values[i];
            });
          },
          currentIndex: _section.index,
        ),
        appBar: new AppBar(title: new Text("Proc")),
        body: _buildBody(),
      );

  Widget _buildBody() {
    switch (_section) {
      case Section.CHECKLISTS:
        return _inbox;
      case Section.TEMPLATES:
        return _templateView;
      default:
        throw "Cannot build body for section $_section";
    }
  }

  Widget _buildInbox() {
    if (_activeChecklists.isEmpty) {
      return new Center(child: new Text("No Active Checklists"));
    }
    return _buildActiveChecklistList();
  }

  Widget _buildActiveChecklistList() => new ListView(
      children: ListTile
          .divideTiles(context: context, tiles: _activeChecklists.map(_buildChecklistListEntry))
          .toList());

  Widget _buildActiveChecklistView() =>
      new Center(child: new Text("TODO: display the checklist here"));

  Widget _buildTemplateList() {
    return new ListView(
        children: ListTile
            .divideTiles(context: context, tiles: _templates.map(_buildTemplateListEntry))
            .toList());
  }

  Widget _buildTemplateListEntry(String template) => new ListTile(
        title: new Text(template),
        onTap: () {
          // TODO: Show template edit screen.
        },
      );

  Widget _buildChecklistListEntry(String checklist) => new ListTile(
        title: new Text(checklist),
        onTap: () {
          Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
            return new Scaffold(
                appBar: new AppBar(title: new Text(checklist)), body: _buildActiveChecklistView());
          }));
        },
      );
}

enum Section { CHECKLISTS, TEMPLATES }

class RandomWordWidget extends StatefulWidget {
  @override
  State createState() => new RandomWordState();
}

class RandomWordState extends State<RandomWordWidget> {
  final _saved = new Set<WordPair>();
  final _suggestions = <WordPair>[];
  final _biggerFont = new TextStyle(fontSize: 18.0);

  @override
  Widget build(BuildContext context) => new Scaffold(
        appBar: new AppBar(
          title: new Text("StartupNameGenerator"),
          actions: <Widget>[new IconButton(icon: new Icon(Icons.list), onPressed: _pushSaved)],
        ),
        body: _buildSuggestions(),
      );

  ListView _buildSuggestions() => new ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (BuildContext buildContext, int row) {
          if (row.isOdd) {
            return Divider();
          }
          final index = row ~/ 2; // not sure why we need to do this?
          if (index >= _suggestions.length) {
            _suggestions.addAll(generateWordPairs().take(10));
          }
          return _buildRow(_suggestions[index]);
        },
      );

  ListTile _buildRow(WordPair wordPair) {
    final wasAlreadySaved = _saved.contains(wordPair);
    return new ListTile(
      title: new Text(wordPair.asPascalCase, style: _biggerFont),
      trailing: new Icon(
        wasAlreadySaved ? Icons.favorite : Icons.favorite_border,
        color: wasAlreadySaved ? Colors.red : null,
      ),
      onTap: () {
        setState(() {
          if (wasAlreadySaved) {
            _saved.remove(wordPair);
          } else {
            _saved.add(wordPair);
          }
        });
      },
    );
  }

  void _pushSaved() {
    Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      final tiles = _saved.map((wordPair) {
        return new ListTile(title: new Text(wordPair.asPascalCase, style: _biggerFont));
      });

      final divided = ListTile.divideTiles(context: context, tiles: tiles).toList();

      return new Scaffold(
        appBar: new AppBar(
          title: new Text("Saved Suggestions"),
        ),
        body: new ListView(children: divided),
      );
    }));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: new Text(widget.title),
      ),
      body: new Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: new Column(
          // Column is also layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug paint" (press "p" in the console where you ran
          // "flutter run", or select "Toggle Debug Paint" from the Flutter tool
          // window in IntelliJ) to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              'You have pushed the button this many times:',
            ),
            new Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: new Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
